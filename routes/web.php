<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('index1');
});

Route::GET('/','UserController@index')->name('index');
Route::GET('backend','Auth\LoginController@show')->name('login');
Route::POST('login','Auth\LoginController@login')->name('login');
Route::GET('logout','Auth\LoginController@logout')->name('logout');
// Auth::routes();
Route::group(['middleware' => ['auth' => 'auth:admin']], function () {
    Route::GET('/dashboard', 'AdminController@dashboard')->name('dashboard');
    Route::POST('admin/add','AdminController@add')->name('admin.add');
    Route::POST('admin/update','AdminController@update')->name('admin.update');
    Route::GET('admin/edit/{id}','AdminController@edit')->name('admin.edit');
    Route::POST('admin/delete/{id}','AdminController@delete')->name('admin.delete');
    Route::POST('admin/change','AdminController@change')->name('admin.change');



});


Route::get('/WinningNumber/2538', function() {
	return view ('/WinningNumber/2538');
});

Route::get('/WinningNumber/2537', function() {
	return view ('/WinningNumber/2537');
});

Route::get('/WinningNumber/2536', function() {
	return view ('/WinningNumber/2536');
});

Route::get('/WinningNumber/2535', function() {
	return view ('/WinningNumber/2535');
});

Route::get('/WinningNumber/2534', function() {
	return view ('/WinningNumber/2534');
});

Route::get('/WinningNumber/2533', function() {
	return view ('/WinningNumber/2533');
});

Route::get('/WinningNumber/2532', function() {
	return view ('/WinningNumber/2532');
});

Route::get('/WinningNumber/2531', function() {
	return view ('/WinningNumber/2531');
});

Route::get('/WinningNumber/2530', function() {
	return view ('/WinningNumber/2530');
});

Route::get('/WinningNumber/2528', function() {
	return view ('/WinningNumber/2528');
});

Route::get('/WinningNumber/2529', function() {
	return view ('/WinningNumber/2529');
});
Route::get('/WinningNumber/2527', function() {
	return view ('/WinningNumber/2527');
});

Route::get('/WinningNumber/2526', function() {
	return view ('/WinningNumber/2526');
});

Route::get('/WinningNumber/2525', function() {
	return view ('/WinningNumber/2525');
});

Route::get('/WinningNumber/2524', function() {
	return view ('/WinningNumber/2524');
});

Route::get('/WinningNumber/2523', function() {
	return view ('/WinningNumber/2523');
});

Route::GET('/result','UserController@result')->name('result');
Route::get('/viewmore/npage', function() {
	return view ('/viewmore/npage');
});

Route::get('/viewmore/index1', function() {
	return view ('/viewmore/index1');
});


Route::get('/viewmore/npage2', function() {
	return view ('/viewmore/npage2');
});


Route::get('/LiveDrawContent/LiveDrawContentTokyo',function(){
	return view('/LiveDrawContent/LiveDrawContentTokyo');
});

// Route::get('/home', 'HomeController@index')->name('home');
